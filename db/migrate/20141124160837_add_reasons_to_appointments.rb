class AddReasonsToAppointments < ActiveRecord::Migration
  def change
    add_column :appointments, :reasons, :string
  end
end
