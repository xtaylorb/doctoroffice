class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :notes
      t.datetime :start
      t.integer :physician_id
      t.integer :patient_id
      t.integer :diagnostic_id

      t.timestamps
    end
  end
end
