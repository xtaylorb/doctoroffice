class Patient < ActiveRecord::Base
  has_many :appointments

  has_many :diagnostics, through: :appointments

  def self.with_appointments
    self.includes(:appointments).where.not(appointments: { id: nil})
  end


  def self.with_appointments_with_diagnostic
    self.includes(:diagnostics).where.not(diagnostics: { id: nil})
  end
end
