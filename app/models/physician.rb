class Physician < ActiveRecord::Base
  has_many :appointments


  def self.with_appointments
    self.includes(:appointments).where.not(appointments: { id: nil})
  end
end
