class Appointment < ActiveRecord::Base

  belongs_to :patient
  belongs_to :physician
  belongs_to :diagnostic

  def self.with_diagnostic
    self.where.not(diagnostic:nil)
  end


  def self.with_diagnostic_for_patient(patient_id)
    self.where.not(diagnostic:nil).where(:patient=>Patient.find(patient_id))
  end

end
