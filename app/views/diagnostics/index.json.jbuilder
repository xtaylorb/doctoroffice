json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :code, :description, :price
  json.url diagnostic_url(diagnostic, format: :json)
end
