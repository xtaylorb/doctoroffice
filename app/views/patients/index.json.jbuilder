json.array!(@patients) do |patient|
  json.extract! patient, :id, :name, :address, :phoneNumber
  json.url patient_url(patient, format: :json)
end
