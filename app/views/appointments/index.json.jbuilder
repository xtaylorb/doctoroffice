json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :notes, :start, :physician_id, :patient_id, :diagnostic_id
  json.url appointment_url(appointment, format: :json)
end
