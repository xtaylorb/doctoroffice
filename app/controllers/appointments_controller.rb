class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]

  # GET /appointments
  # GET /appointments.json
  def index
    @appointments = Appointment.all
  end


  def patient_appointments
    @appointments = Appointment.where(:patient => Patient.find(params[:id]))
  end

  def patient_appointments_with_diagnostic
    @appointments = Appointment.with_diagnostic_for_patient(params[:id])
  end

  def physician_appointments_with_invoiced_amount
    @appointments = Appointment.with_diagnostic.order("physician_id asc")
  end


  def appointment_invoice
    appointment= Appointment.find(params[:id])
    html = render_to_string(:layout => "appointment_invoice.html", :locals => {:appointment => appointment})
    pdf=WickedPdf.new.pdf_from_string(html)
    send_data(pdf, :filename => "invoice"+appointment.id.to_s+".pdf", :disposition => 'attachment')
  end

  def physician_appointments
    @appointments = Appointment.where(:physician => Physician.find(params[:id]))
  end

  # GET /appointments/1
  # GET /appointments/1.json
  def show
  end

  # GET /appointments/new
  def new
    @appointment = Appointment.new
  end

  # GET /appointments/new_patient
  def new_patient
    @appointment = Appointment.new
  end


  # GET /appointments/1/edit
  def edit
  end

  # POST /appointments
  # POST /appointments.json
  def create

    @appointment = Appointment.new(appointment_params)
    respond_to do |format|
      if @appointment.save
        format.html { redirect_to @appointment, notice: 'Appointment was successfully created.' }
        format.json { render :show, status: :created, location: @appointment }
      else
        format.html { render :new }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end


  def occupied_date(new_app)
    occupied=false
    Appointment.where(:physician_id == new_app.physician_id).each do |app|

      timediff=TimeDifference.between(new_app.start,app.start).in_minutes.abs
      puts "time diff:"+timediff.to_s+ " new_app"+new_app.start.to_s+ " other app:"+app.start.to_s
      if ((timediff)) < 30
        occupied=true
      end
    end
    return occupied
  end

  # POST /appointments
  # POST /appointments.json
  def patient_create
    @appointment = Appointment.new(appointment_params)
    hour=params["hour"].to_i
    year=params["year"].to_i
    month=params["month"].to_i
    day=params["day"].to_i

    puts "---------year:"+year.to_s
    puts "---------month:"+month.to_s
    puts "---------day:"+day.to_s

    minutes=params["minutes"].to_i
    start=DateTime.new(year,month,day,hour,minutes,0,'+0')
    @appointment.start=start

    respond_to do |format|
      #validate the date
      if @appointment.start.saturday? || @appointment.start.sunday?

        @appointment.errors.add(:start, "Appointment cannot be on weekend")
        format.html { render :new_patient }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }

        #past date


      elsif @appointment.start.past?
        @appointment.errors.add(:start, "past date")
        format.html { render :new_patient }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      elsif occupied_date(@appointment)
        @appointment.errors.add(:start, "Occupied date")
        format.html { render :new_patient}
        format.json { render json: @appointment.errors, status: :unprocessable_entity }

        elsif @appointment.save
        format.html { redirect_to @appointment, notice: 'Appointment was successfully created.' }
        format.json { render :show, status: :created, location: @appointment }
      else
        format.html { render :new_patient }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /appointments/1
  # PATCH/PUT /appointments/1.json
  def update
    respond_to do |format|
      if @appointment.update(appointment_params)
        format.html { redirect_to @appointment, notice: 'Appointment was successfully updated.' }
        format.json { render :show, status: :ok, location: @appointment }
      else
        format.html { render :edit }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appointments/1
  # DELETE /appointments/1.json
  def destroy
    @appointment.destroy
    respond_to do |format|
      format.html { redirect_to appointments_url, notice: 'Appointment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_appointment
    @appointment = Appointment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def appointment_params
    params.require(:appointment).permit(:notes, :start, :physician_id, :patient_id, :diagnostic_id)
  end
end
